# Test2

This directory contains solutions for test 2.

## Get Started

```bash
# install expo cli
npm i -g expo-cli
```

```bash
# install dependencies
npm i
```

To run the project, run the following commands:

```bash
npm run start

# or directly open the emulator
npm run android
```
