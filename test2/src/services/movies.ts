import api from './api';

// since it's only used temporarily
// so it shouldn't matter if it's shown?
const apiKey = '1f749e865bf0e73125dfcc507fae41ba';

export default {
  getUpcomingMovieList() {
    return api.get(`/movie/upcoming?api_key=${apiKey}&language=en-US&page=1`);
  },
  getPopularMovieList() {
    return api.get(`/movie/popular?api_key=${apiKey}&language=en-US&page=1`);
  },
  getDetail(id: string) {
    return api.get(`/movie/${id}?api_key=${apiKey}&language=en-US`);
  }
};
