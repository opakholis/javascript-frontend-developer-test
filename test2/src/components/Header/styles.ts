import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  avatar: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 15
  },
  name: {
    fontSize: 16,
    fontFamily: 'Poppins_700Bold'
  },
  description: {
    fontSize: 14,
    fontFamily: 'Poppins_400Regular'
  }
});
