import { Image } from 'react-native';

import { Text, View } from '../Themed';
import { styles } from './styles';

const portrait = 'https://opkhls.dev/portrait';
const user = 'opakholis';

export default function Header() {
  return (
    <View style={styles.container}>
      <Image source={{ uri: portrait }} style={styles.avatar} />
      <View>
        <Text style={styles.name}>Hello, {user}</Text>
        <Text style={styles.description}>
          Let's stream your favorite movies
        </Text>
      </View>
    </View>
  );
}
