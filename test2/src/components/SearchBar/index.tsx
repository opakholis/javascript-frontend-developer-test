import { Ionicons } from '@expo/vector-icons';
import * as React from 'react';
import { TextInput, TextInputProps } from 'react-native';

import { View } from '../../components/Themed';
import { styles } from './styles';

export function SearchBar(props: TextInputProps) {
  return (
    <View style={styles.inputWrapper} lightColor="#ddd" darkColor="#181a1e">
      <Ionicons name="search" size={20} color="gray" style={styles.icon} />
      <TextInput
        style={styles.input}
        placeholder="Search a title.."
        placeholderTextColor="gray"
        {...props}
      />
    </View>
  );
}
