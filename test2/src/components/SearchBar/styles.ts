import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  inputWrapper: {
    marginVertical: 15,
    flexDirection: 'row',
    alignItems: 'center',
    width: '100%',
    paddingHorizontal: 10,
    borderRadius: 20
  },
  icon: {
    marginLeft: 10,
    marginRight: 5
  },
  input: {
    flexGrow: 1,
    padding: 10,
    fontSize: 16,
    color: 'gray'
  }
});
