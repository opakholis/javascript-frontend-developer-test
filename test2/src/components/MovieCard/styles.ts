import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  card: {
    width: 150,
    height: 200,
    marginRight: 10
  },
  poster: {
    width: '100%',
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    height: '100%'
  },
  caption: {
    justifyContent: 'center',
    width: '100%',
    height: 45,
    padding: 8,
    borderBottomLeftRadius: 10,
    borderBottomRightRadius: 10
  },
  title: {
    fontSize: 16,
    fontWeight: 'bold',
    fontFamily: 'Poppins_700Bold'
  }
});
