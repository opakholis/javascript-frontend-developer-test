import { useNavigation } from '@react-navigation/native';
import { Image, Pressable } from 'react-native';

import { View, Text } from '../../components/Themed';
import { styles } from './styles';

export interface IMovie {
  id: string;
  title: string;
  poster_path: string;
}

export function MovieCard({ data }: { data: IMovie }) {
  const navigation = useNavigation();

  const navigateToDetailScreen = () => {
    navigation.navigate('DetailMovie', { id: data.id });
  };

  return (
    <Pressable style={styles.card} onPress={navigateToDetailScreen}>
      <Image
        source={{ uri: `https://image.tmdb.org/t/p/w500${data.poster_path}` }}
        style={styles.poster}
        resizeMode="cover"
      />
      <View style={styles.caption} lightColor="#ddd" darkColor="#181a1e">
        <Text style={styles.title} numberOfLines={1}>
          {data.title}
        </Text>
      </View>
    </Pressable>
  );
}
