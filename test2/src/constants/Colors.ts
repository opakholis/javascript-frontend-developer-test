export const tintColorLight = '#2f95dc';
export const tintColorDark = '#fff';

export const gradients = {
  black: ['rgba(0,0,0,0.8)', 'rgba(0,0,0,0.4)', 'rgba(0,0,0,1)'],
  white: [
    'rgba(255,255,255,0.8)',
    'rgba(255,255,255,0.1)',
    'rgba(255,255,255,1)'
  ]
};

export default {
  light: {
    text: '#000',
    background: '#fff',
    tint: tintColorLight,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorLight
  },
  dark: {
    text: '#fff',
    background: '#000',
    tint: tintColorDark,
    tabIconDefault: '#ccc',
    tabIconSelected: tintColorDark
  }
};
