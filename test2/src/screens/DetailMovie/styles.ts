import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20
  },
  imageBackground: {
    width: '100%',
    height: 400,
    position: 'absolute',
    top: -50
  },
  image: {
    marginTop: 80,
    height: 300,
    width: 200,
    borderRadius: 10
  },
  linearGradient: {
    width: '100%',
    height: '100%'
  },
  textHeading: {
    fontSize: 20,
    fontFamily: 'Poppins_700Bold',
    marginVertical: 10
  },
  textDescription: {
    fontSize: 16,
    lineHeight: 24,
    fontFamily: 'Poppins_400Regular'
  },
  loading: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20
  },
  // aditional information
  info: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'center'
  },
  infoItem: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 10
  },
  infoText: {
    fontSize: 16,
    fontFamily: 'Poppins_400Regular',
    marginLeft: 5
  }
});
