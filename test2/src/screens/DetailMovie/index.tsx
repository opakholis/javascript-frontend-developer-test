import { MaterialCommunityIcons } from '@expo/vector-icons';
import { useRoute, useNavigation } from '@react-navigation/native';
import { LinearGradient } from 'expo-linear-gradient';
import { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Image,
  ImageBackground,
  useColorScheme
} from 'react-native';

import { getHours } from '../../utils/getHours';
import { View, Text } from '../../components/Themed';
import { gradients } from '../../constants/Colors';
import movies from '../../services/movies';
import { styles } from './styles';

interface IDetailMovie {
  id: number;
  overview: string;
  poster_path: string;
  backdrop_path: string;
  release_date: string;
  title: string;
  runtime: number;
  vote_average: number;
}

export function DetailMovie() {
  const route = useRoute<any>();
  const navigation = useNavigation();
  const colorScheme = useColorScheme();
  const [movie, setMovie] = useState<IDetailMovie | null>(null);

  const { id } = route.params;

  useEffect(() => {
    // initiation when calling the wrong id
    if (!id) return navigation.goBack();

    fetchDetailMovie();
  }, [id]);

  const fetchDetailMovie = async () => {
    try {
      const res = await movies.getDetail(id);
      const data = res.data;
      setMovie(data);
    } catch (error) {
      console.log(error);
    }
  };

  console.log(movie);

  return !movie ? (
    <View style={[styles.container, styles.loading]}>
      <ActivityIndicator size="large" />
    </View>
  ) : (
    <View style={{ flex: 1 }}>
      <View style={styles.imageContainer}>
        <ImageBackground
          style={styles.imageBackground}
          source={{
            uri: `https://image.tmdb.org/t/p/w500${movie.backdrop_path}`
          }}
        >
          <LinearGradient
            colors={colorScheme === 'dark' ? gradients.black : gradients.white}
            style={styles.linearGradient}
          />
        </ImageBackground>
        <Image
          style={styles.image}
          source={{
            uri: `https://image.tmdb.org/t/p/w500${movie.poster_path}`
          }}
        />
      </View>
      <View style={styles.container}>
        <View style={styles.info}>
          <View style={styles.infoItem}>
            <MaterialCommunityIcons name="clock" size={24} color="gray" />
            <Text style={styles.infoText}>{getHours(movie.runtime)}</Text>
          </View>
          <View style={styles.infoItem}>
            <MaterialCommunityIcons
              name="calendar-today"
              size={24}
              color="gray"
            />
            <Text style={styles.infoText}>
              {new Date(movie.release_date).getFullYear()}
            </Text>
          </View>
          <View style={styles.infoItem}>
            <MaterialCommunityIcons name="star" size={24} color="gray" />
            <Text style={styles.infoText}>{movie.vote_average}</Text>
          </View>
        </View>
        <Text style={styles.textHeading}>Story Line</Text>
        <Text style={styles.textDescription} numberOfLines={6}>
          {movie?.overview}
        </Text>
      </View>
    </View>
  );
}
