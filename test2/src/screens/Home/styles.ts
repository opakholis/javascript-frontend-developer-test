import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15
  },
  headerWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 15
  },
  headerText: {
    fontSize: 20,
    fontFamily: 'Poppins_700Bold'
  },
  headerLink: {
    fontSize: 16,
    fontFamily: 'Poppins_400Regular'
  },
  cardSection: {
    height: 245
  }
});
