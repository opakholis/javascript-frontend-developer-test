import { useEffect, useState } from 'react';
import { FlatList, ListRenderItem } from 'react-native';

import { IMovie, MovieCard } from '../../components/MovieCard';
import { SearchBar } from '../../components/SearchBar';
import { View, Text } from '../../components/Themed';
import movies from '../../services/movies';
import { styles } from './styles';

export default function HomeScreen() {
  const [movie, setMovie] = useState([]);

  useEffect(() => {
    getMovies();
  }, []);

  const getMovies = async () => {
    try {
      const res = await movies.getPopularMovieList();
      const data = res.data.results;
      setMovie(data);
    } catch (error) {
      console.log(error);
    }
  };

  const renderItem: ListRenderItem<IMovie> = ({ item }) => (
    <MovieCard data={item} />
  );

  return (
    <View style={styles.container}>
      <SearchBar />
      <View style={styles.headerWrapper}>
        <Text style={styles.headerText}>Most Popular</Text>
        <Text style={styles.headerLink}>See All</Text>
      </View>
      <View style={styles.cardSection}>
        <FlatList
          horizontal
          data={movie.slice(0, 7)}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item: IMovie) => item.id}
          renderItem={renderItem}
        />
      </View>
    </View>
  );
}
