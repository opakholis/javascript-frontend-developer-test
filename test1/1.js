function findDuplicate(arr) {
  let map = new Map();
  for (let i = 0; i < arr.length; i++) {
    /*
     * If the value is already present in the map,
     * then we have found the duplicate element.
     */
    if (map.has(arr[i])) return arr[i];
    /*
     * make sure we store the value in the map first
     */
    map.set(arr[i]);
  }
  /*
   * If we reach here, then no duplicate element was found.
   */
  return '"meh"';
}

const val = [1, 2, 3, 4, 4];
console.log(`the duplicate element is ${findDuplicate(val)}`);
