function combinations(arr) {
  let result = [];

  for (let i = 0; i < arr.length; i++) {
    for (let j = i; j < arr.length; j++) {
      result.push([arr[i], arr[j]]);
    }
  }
  return result;
}

const val = [1, 2, 3];
console.log(combinations(val));
