function sortArray(arr) {
  let zero = 0;
  let one = 0;
  let two = 0;
  let result = [];

  /*
   * make a loop to iterate through the array
   * and assign the value to the corresponding variable
   */
  for (let i = 0; i < arr.length; i++)
    if (arr[i] === 0) zero++;
    else if (arr[i] === 1) one++;
    else two++;

  /*
   * make sure if the value is 0, then push 0 to the result array
   * and so on
   */
  for (let i = 0; i < zero; i++) result.push(0);
  for (let i = 0; i < one; i++) result.push(1);
  for (let i = 0; i < two; i++) result.push(2);

  return result;
}

const val = [0, 1, 2, 2, 1, 0, 0, 2, 0, 1, 1, 0];
console.log(sortArray(val));
